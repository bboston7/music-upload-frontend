"use strict";

/** TODO:
 * Whole page should just be "Drag to upload anywhere in this page or use the
 * file selector", then after submission / drop, replace entire page with
 * progress table.  After finishing, have a button to go back and upload more
 */

const kSuffixes = ["B", "KiB", "MiB", "GiB"];

// Global representing the number of currently running uploads
var uploads_running = 0;

// Global representing whether any uploads failed
var uploads_failed = false;

// Convert an integer `bytes` into a human readable power of 2 based string
// representation
function toHumanReadable(bytes) {
  for (var i = 0; i < kSuffixes.length; ++i) {
    if (bytes < 1024) return bytes.toFixed(1) + kSuffixes[i];
    bytes /= 1024;
  }
  return bytes.toFixed(1) + kSuffixes[kSuffixes.length - 1];
}

function upload(file) {
  // Create row in table and fill it out
  var output = document.getElementById("output");
  output.hidden = false;
  var row = output.insertRow(-1);
  var filename = row.insertCell(-1);
  var file_status = row.insertCell(-1);
  filename.innerHTML = file.name;
  var progress = document.createElement("progress");
  var status_text = document.createElement("span");
  file_status.appendChild(status_text);
  file_status.appendChild(progress);
  output.appendChild(row);
  var request = new XMLHttpRequest();

  // Bind form data boject to form element
  var data = new FormData();
  data.set('song', file);

  // Define what happens on successful submission
  request.addEventListener("load", function(event) {
    var output = document.getElementById("output");
    var response = JSON.parse(event.target.responseText);
    var http_status = event.target.status;
    --uploads_running;
    if (http_status === 200) {
      file_status.innerHTML = "Successfully uploaded " +
                              response.title +
                              " by " +
                              response.artist;
      if (!uploads_running && !uploads_failed) {
        document.getElementById("after-upload").hidden = false;
      }
    } else if (http_status === 400 || http_status === 500) {
      file_status.innerHTML = "Your file has been rejected for the following reason: " +
                              response.message;
      document.getElementById("errors").hidden = false;

      // Can't put borders on rows, so must put them on each cell.  CSS
      // collapses borders
      filename.classList.add("error-left");
      file_status.classList.add("error-right");
      uploads_failed = true;
    } else {
      output.innerHTML = "ERROR: Unknown status code from server: " +
                         http_status;
      uploads_failed = true;
    }

    if (!uploads_running) {
      document.getElementById("restart").hidden = false;
    }
  });

  // Define what happens  in case of error
  request.addEventListener("error", function(event) {
    document.getElementById("connection-error").hidden = false;
  });

  // Track progress of upload
  request.upload.onprogress = function(event) {
    progress.value = event.loaded;
    progress.max = event.total;
    status_text.innerHTML = toHumanReadable(event.loaded) +
                            " / " +
                            toHumanReadable(event.total);
  };

  // Set up request
  request.open("POST", "http://localhost:8888/upload")

  // Send data
  request.send(data);
}

// Bind a listener to submit on the form and hijack the event
function bind_form() {
  // Access form
  var form = document.getElementById("form");

  // Reset data in form (in case of reload)
  form.reset();

  // Access files
  var file_input = document.getElementById("file-input");

  // Access output list
  var output = document.getElementById("output");

  // Hijack submit event
  form.addEventListener("submit", function(event) {
    // Stop default submit behavior
    event.preventDefault();
    document.getElementById("instructions").remove();

    var files = file_input.files;
    uploads_running += files.length;
    for (var i = 0; i < files.length; ++i) {
      // Upload files individually
      upload(files[i]);
    }
  });
}

function prevent_defaults(event) {
  event.preventDefault();
  event.stopPropagation();
}

/**
 * Highlight the drop area
 */
function highlight(event) {
  document.body.classList.add("highlight");
}

/**
 * Remove drop area highlight
 */
function unhighlight(event) {
  document.body.classList.remove("highlight");
}

function bind_drag_and_drop() {
  // Access drag and drop area
  //let drop_area = document.getElementById("drag-drop");

  // Block default actions on drag/drop
  ["dragenter", "dragover", "dragleave", "drop"].forEach(event_type => {
    //drop_area.addEventListener(event_type, prevent_defaults, false);

    // TODO: Do I really want to disable this everywhere?  Do I need to add it
    // to event area if I add it to document?  Should I also bind the "drop"
    // event to the entire page for ease of use / to not punish those who
    // aren't as technically competent?
    document.addEventListener(event_type, prevent_defaults, false);
  });

  // Highlight when dragging files into div
  ["dragenter", "dragover"].forEach(event_type => {
    document.addEventListener(event_type, highlight, false);
  });

  // Unhighlight when dragging files out of div
  ["dragleave", "drop"].forEach(event_type => {
    document.addEventListener(event_type, unhighlight, false);
  });


  document.addEventListener("drop", function(event) {
    let instructions = document.getElementById("instructions");
    if (instructions !== null) instructions.remove();
    let files = event.dataTransfer.files;
    uploads_running += files.length;
    for (let i = 0; i < files.length; ++i) {
      // Upload files individually
      upload(files[i]);
    }
    console.log(files);
  });
}

window.addEventListener("load", function() {
  bind_form();
  bind_drag_and_drop();
});
